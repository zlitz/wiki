import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts.component/posts.component';

const POST_ROUTES: Routes = [
  ...routeBuilder(20, '', PostsComponent)
];
export const POSTS_ROUTING: ModuleWithProviders = RouterModule.forChild(POST_ROUTES);

/**
 * Create an array or routes where each path is like:
 * :folder0/:file, :folder0/:folder1/:file, :folder0/:folder1/:folder2/:file, ...
 */
function routeBuilder(depth: number, root: string, component: any): any[] {

  let retVal: { path: string, component: any }[] = [];

  for(let i = 1; i <= depth; i++) {

    let currentPathParts: string[] = (root || '').length > 0 ?  [root] : [];
    
    for(let j = 0; j < i; j++) {

      let isLast = j === (i - 1);
      let pathPart = isLast ? ':file' : `:folder${j}`;

      currentPathParts.push(pathPart);
    }

    let routePath = currentPathParts.join('/');
    retVal.push({ path: routePath, component: component });
  }

  return retVal;
}