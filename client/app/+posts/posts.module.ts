import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';

import { POSTS_ROUTING } from './posts.routing';
import { PostsComponent } from './posts.component/posts.component';

@NgModule({
    imports:  [ 
        POSTS_ROUTING,
        CommonModule
    ],
    declarations: [ 
        PostsComponent
    ],
    entryComponents: [ PostsComponent ]
})
export class PostsModule { }
