import {
    Component,
    OnInit
} from '@angular/core';

import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    template: '<div>Hello</div>'
})
export class PostsComponent implements OnInit {

    //--------------------------------------------------
    // Injected
    //--------------------------------------------------
    private _route: ActivatedRoute;

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor(route: ActivatedRoute) {
        this._route = route;
    }



    /**
     * NG2 life-cycle 
     */
    ngOnInit() {
        this._route.params.forEach((params: Params) => {
            let filePath =  this.resolveRouteParams(params);
        });
    }

    //====================================================================================================
    // PRIVATE
    //====================================================================================================
    /**
     * Resolve the route params to a post path
     */
    private resolveRouteParams(params: Params): string {

        let keys = Object.keys(params);
        let folders: { order: number; name: string }[] = [];
        let file: string = '';

        for(let i = 0; i < keys.length; i++) {

            let key = keys[i];
            
            let folderRegexMatch = /folder(\d{1,4})/.exec(key);

            if (folderRegexMatch) {
                folders.push({ order: parseInt(folderRegexMatch[1]), name: params[key] });
                continue;
            }

            let isFile = key === 'file';

            if (isFile) {
                file = params[key];
            }
        }

        let pathParts = folders.sort((a,b) => a.order > b.order ? 1 : -1).map(a => a.name);
        pathParts.push(file);

        return pathParts.join('/') ;
    }
}