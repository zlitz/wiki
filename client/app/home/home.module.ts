import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HomeComponent } from './home.component/home.component';

export { HomeComponent } from './home.component/home.component';

@NgModule({
  imports:  [ 
    BrowserModule,
  ],
  declarations: [ 
      HomeComponent
  ],
  providers: [
  ]
})
export class HomeModule { }
