import { 
    Component 
} from '@angular/core';

import { RoutingService } from 'app/routing';

@Component({
    template: '<div>Home</div>'
})
export class HomeComponent {

    //--------------------------------------------------
    // Inject
    //--------------------------------------------------
    private _routingService: RoutingService;

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor(routingService: RoutingService) {
        this._routingService = routingService;

        // setTimeout(() => this._routingService.gotoRoute(['/admin']), 5000)
    }
}