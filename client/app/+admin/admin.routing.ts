import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component/admin.component';

const ADMIN_ROUTES: Routes = [
  {
    path: '',
    component: AdminComponent,
  }
];
export const ADMIN_ROUTING: ModuleWithProviders = RouterModule.forChild(ADMIN_ROUTES);
