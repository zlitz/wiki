import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';

import { ADMIN_ROUTING } from './admin.routing';
import { AdminComponent } from './admin.component/admin.component';

@NgModule({
    imports:  [ 
        ADMIN_ROUTING,
        CommonModule
    ],
    declarations: [ 
        AdminComponent
    ],
    entryComponents: [ AdminComponent ]
})
export class AdminModule { }
