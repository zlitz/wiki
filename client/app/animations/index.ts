import {
    trigger,
    state,
    style,
    transition,
    animate
} from '@angular/core';


export function openVertical(triggerName: string, closedState: string, openedState: string, duration: number) {

    return trigger(triggerName, [
        state(closedState, style({
            height: 0,
            overflow: 'hidden'
        })),
        state(openedState, style({
            height: '*'
        })),
        transition(`${closedState} => ${openedState}`, animate(`${duration}ms ease-in`)),
        transition(`${openedState} => ${closedState}`, animate(`${duration}ms ease-out`)),
    ]);
}