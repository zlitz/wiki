import {
    Component
} from '@angular/core';

import { openVertical } from 'app/animations';

@Component({
    selector: 'zl-app-side-menu',
    templateUrl: 'app/app/side-menu.component/side-menu.component.html',
    styleUrls: ['app/app/side-menu.component/side-menu.component.css'],
    animations: [openVertical('openSection', 'void', '*', 200)]
})
export class SideMenuComponent {

    //--------------------------------------------------
    // Public
    //--------------------------------------------------
    public vm: IViewModel;

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor() {
        this.buildViewModel();
    }



    //====================================================================================================
    // PRIVATE
    //====================================================================================================
    /**
     * 
     */
    private buildViewModel(): void {

        this.vm = {
            onClick_section: (section: IMenuSection) => {

                if (this.vm.openedSection === section) {
                    this.vm.openedSection = null;
                } else {
                    this.vm.openedSection = section;
                }

            },
            sections: [
                {
                    name: "Section 1 asdflak;df;akldfh",
                    items: [
                        { isRoute: true, link: 'foo', text: 'Link 1 abcdefghijkmfasdfasd'},
                        { isRoute: true, link: 'foo', text: 'Link 2'},
                        { isRoute: true, link: 'foo', text: 'Link 3'},
                        { isRoute: true, link: 'foo', text: 'Link 4'},
                    ]
                },
                {
                    name: "Section 2",
                    items: [
                        { isRoute: true, link: 'foo', text: 'Link 1'},
                        { isRoute: true, link: 'foo', text: 'Link 2'},
                        { isRoute: true, link: 'foo', text: 'Link 3'},
                        { isRoute: true, link: 'foo', text: 'Link 4'},
                    ]
                }
            ]
        }
    }
}


interface IMenuSection {
    name: string;
    items: IMenuItem[];
}

interface IMenuItem {
    text: string;
    isRoute: boolean;
    link: string;
}

interface IViewModel {
    onClick_section(section: IMenuSection): void;
    openedSection?: IMenuSection | null;
    sections: IMenuSection[];   
}