import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'app/home';

const APP_ROUTES: Routes = [
    { 
        path: 'admin',
        loadChildren: 'app/+admin/admin.module#AdminModule'
    }, 
    {
        path: 'posts',
        loadChildren: 'app/+posts/posts.module#PostsModule'
    },
    {
        path: '',
        component: HomeComponent
    }
];

export const APP_ROUTING_PROVIDERS: any[] = [

];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
