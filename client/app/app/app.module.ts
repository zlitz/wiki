import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_ROUTING_PROVIDERS, ROUTING} from './app.routing';
import { AppComponent }   from './app.component/app.component';
import { SideMenuComponent } from './side-menu.component/side-menu.component';
import { HomeModule } from 'app/home';
import { RoutingModule } from 'app/routing';

@NgModule({
  imports:  [ 
    BrowserModule,
    HomeModule,
    ROUTING,
    RoutingModule
  ],
  declarations: [ 
    AppComponent,
    SideMenuComponent
  ],
  bootstrap: [ AppComponent ],
  providers: [
    APP_ROUTING_PROVIDERS
  ]
})
export class AppModule { }
