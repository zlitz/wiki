import { NgModule }      from '@angular/core';
import { RouterModule } from '@angular/router';

import { RoutingService } from './routing.service';

export { RoutingService } from './routing.service';

@NgModule({
    imports:  [ 
        RouterModule
    ],
    declarations: [ 
        
    ],
    providers: [
        RoutingService
    ]
})
export class RoutingModule { }
