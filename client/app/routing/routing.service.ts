import { Injectable } from '@angular/core'
import { Router } from '@angular/router';

@Injectable()
export class RoutingService {

    //--------------------------------------------------
    // Injected
    //--------------------------------------------------
    private _router: Router;

    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor(router: Router) {
        this._router = router;
    }



    /**
     * 
     */
    public getAdminRoute(getUrl: string): string | any[] {
        
        if (getUrl) {
            return '/admin';
        }
        
        return ['/admin'];
    }



    /**
     * argument is route and arguments.
     * E.g., ['/home', userId]
     */
    public gotoRoute(route: any[]): void {
        this._router.navigate(route);
    }
}