import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as path from 'path';


export class Server {

    //--------------------------------------------------
    // Private
    //--------------------------------------------------
    private _server: express.Application;


    //====================================================================================================
    // PUBLIC
    //====================================================================================================
    public constructor() {
        this._server = express();

        this.init();
    }



    /**
     * 
     */
    public startServer(port: number) {
        this._server.listen(port, function () {
            console.log(`Listening on port ${port}!`);
        });
    }



    //====================================================================================================
    // PRIVATE
    //====================================================================================================
    
    
    
    /**
     * 
     */
    private init(): void {

        //Setup body parser
        this._server.use(bodyParser.urlencoded({ extended: true }));
        this._server.use(bodyParser.json());

        //Register API routes
        //TODO

        //Setup static files

        //First we look for static resource in client dir
        let staticDir = path.normalize(__dirname + '/../../client/');
        this._server.use(express.static(staticDir));

        //Now we load app start point if the static server fails
        this._server.use('*', (request, response, next) => {
            response.sendFile(staticDir + 'index.html');
        });
    }
}